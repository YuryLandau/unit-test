import { testObject } from '../data/object'

describe("Testando um objeto", () => {

    test('O tipo de dado deve ser um Array', () => {
        expect(testObject).toBeInstanceOf(Array)
    })

    testObject.map((indice) => {

        test('O indice deve ser do tipo Objeto', () => {
            console.log(typeof(indice.name))
            expect(indice.name).toBeInstanceOf(String)
        })
    })
    
})