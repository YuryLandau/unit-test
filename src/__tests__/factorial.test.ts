import getFactorial from '../data/factorialTS'

const testObject = [
    {
        name: "yury1",
        secondName: "landau1"
    },
    {
        name: "yury2",
        secondName: "landau2"
    },
    {
        name: "yury3",
        secondName: "landau3"
    },
    {
        name: "yury4",
        secondName: "landau4"
    },
]

const describePart = [
    "teste1",
    "teste2",
    "teste3",
    "teste4",
]

testObject.map((testStep) => {
    
    describe(testStep.name, () => {
    
        test('getFactorial should be a function', () => {
            expect(getFactorial).toBeInstanceOf(Function)
        })
        
        test('getFactorial(4) should return 24', () => {
            const expected = 24;
            const actual = getFactorial(4)
            
            expect(actual).toBe(expected)
        })
    })
})

describePart.map((testStep) => {
    
    describe(testStep, () => {
    
        it('getFactorial should be a function', () => {
            expect(getFactorial).toBeInstanceOf(Function)
        })
        
        it('getFactorial(4) should return 24', () => {
            const expected = 24;
            const actual = getFactorial(4)

            expect(actual).toBe(expected)
        })
    })
})
